package com.example.clase2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.index.*

class MainActivity : AppCompatActivity() {

    lateinit var correo : EditText
    lateinit var contraseña : EditText
    lateinit var ingresar : Button

    val map:HashMap<String,String> = hashMapOf("user@gmail.com" to "123", "user2@gmail.com" to "contraseña", "com1pac56@hotmail.com" to "123456789")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*setContentView(R.layout.login);

        //Contiene las referecias a los objetos creados en la vista
        correo = findViewById(R.id.et_correo)
        contraseña = findViewById(R.id.et_contrasena)
        ingresar = findViewById(R.id.bt_ingresar)

        //Cada vez que se presiona el botón ejecuta el código de abajo
        ingresar.setOnClickListener {
            if(map[correo.text.toString()] == contraseña.text.toString())
            {
                Toast.makeText(this,"Inicio de sesión exitoso.", Toast.LENGTH_SHORT).show()
            }
            else
            {
                Toast.makeText(this,"Inicio de sesión fallido.", Toast.LENGTH_SHORT).show()
            }
        }*/

        setContentView(R.layout.index)

        val quizzes = ArrayList<Quizz>()

        quizzes.add(Quizz(R.drawable.dogo, "Animals", R.drawable.padlock))
        quizzes.add(Quizz(R.drawable.cat, "Animals", R.drawable.padlock))
        quizzes.add(Quizz(R.drawable.lion, "Animals", R.drawable.padlock))

        rv_quizz.layoutManager = LinearLayoutManager(this)
        rv_quizz.adapter = QuizAdapter(quizzes, object : ClickListener{
            override fun onClick(view: View, position: Int) {
                //Toast.makeText(applicationContext, quizzes[position].nombreQuizz, Toast.LENGTH_SHORT).show()
                val intento = Intent(applicationContext, Content::class.java)
                intento.putExtra("quizname",quizzes[position].nombreQuizz)
                startActivity(intento)
            }
        })

    }

    override fun onPause() {
        super.onPause()
        mostrarMensaje("En Pausa")
    }

    override fun onResume() {
        super.onResume()
        mostrarMensaje("En Resumen")
    }

    override fun onDestroy() {
        super.onDestroy()
        mostrarMensaje("En Destrucción")
    }

    override fun onStart() {
        super.onStart()
        mostrarMensaje("En Start")
    }

    override fun onStop() {
        super.onStop()
        mostrarMensaje("En Stop")
    }

    fun mostrarMensaje(mensaje:String) = Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
}