package com.example.clase2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_content.*

class Content : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        //Intents explícitos
        //Serializable investigar**
        textView.text = intent.getStringExtra("quizname")
    }
}
//Mayalo
//Deepl

//SharedPrefereces
//google.com.gsun
//Start Activity For Result